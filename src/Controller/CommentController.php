<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Ticket;
use App\Form\CommentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/comment')]
class CommentController extends AbstractController
{
    #[Route('/delete/{id}', name: 'app_comment_delete', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    #[IsGranted('delete', 'comment')]
    public function delete(#[\SensitiveParameter] Comment $comment, EntityManagerInterface $entityManager, Request $request): Response
    {
        try {
            $entityManager->remove($comment);
            $entityManager->flush();
            $this->addFlash('success', 'Your comment has successfully deleted !');
        } catch (\Exception|\Throwable $exception) {
            $this->addFlash('error', 'Something went wrong, your comment has not been deleted.');
        } finally {
            return $this->redirect(
                $request->headers->get('referer')
            );
        }
    }

    #[Route('/update/{id}', name: 'app_comment_update', methods: ["GET", "POST"])]
    #[IsGranted('ROLE_USER')]
    #[IsGranted('edit', 'comment')]
    public function update(#[\SensitiveParameter] Comment $comment, EntityManagerInterface $entityManager, Request $request): Response
    {
        $form = $this->createForm(type: CommentType::class, data: $comment);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $comment = $form->getData();

            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute(route: 'ticket_read', parameters: [
                'id' => $comment->getTicket()->getId()
            ]);
        }

        return $this->render(view: 'comment/update.html.twig', parameters: [
            'comment' => $comment
        ]);
    }

    #[Route('/create/{id}', name: 'app_comment_create', methods: ["GET", "POST"])]
    #[IsGranted('ROLE_USER')]
    public function create(#[\SensitiveParameter] Ticket $ticket, EntityManagerInterface $entityManager, Request $request): Response
    {
        $form = $this->createForm(type: CommentType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $comment->setTicket($ticket);

            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute(route: 'ticket_read', parameters: [
                'id' => $ticket->getId()
            ]);
        }

        return $this->render('comment/create.html.twig', [
            'ticket' => $ticket,
            'form' => $form
        ]);
    }
}
