<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Form\TagItemType;
use App\Form\TagListType;
use App\Repository\TagRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/tags')]
class TagController extends AbstractController
{

    #[Route('/', name: 'app_tags_index', methods: ["GET"])]
    public function index(TagRepository $tagRepository): Response
    {
        $tags = $tagRepository->findAll();

        return $this->render(
            view: 'tag/index.html.twig',
            parameters: [
                'tags' => $tags
            ]
        );
    }

    #[Route('/create', name: 'app_tag_create', methods: ["GET", "POST"])]
    #[IsGranted('ROLE_ADMIN')]
    public function create(Request $request): Response
    {
        $form = $this->createForm(type: TagListType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
        }

        return $this->render(
            view: 'tag/create.html.twig',
            parameters: [
                'form' => $form
            ]
        );
    }

    #[Route('/show/{id}', name: 'app_tags_ticket', methods: ["GET"])]
    public function questionByTag(#[\SensitiveParameter] Tag $tag): Response
    {
        return $this->render(
            view: 'ticket/index.html.twig',
            parameters: [
                'tickets' => $tag->getTickets()
            ]
        );
    }

    #[Route('/delete/{id}', name: 'app_tag_delete', methods: ["GET"])]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(#[\SensitiveParameter] Tag $tag, EntityManagerInterface $entityManager): Response
    {
        $entityManager->remove($tag);
        $entityManager->flush();

        return $this->redirectToRoute(
            route: 'app_tags_index'
        );
    }

    #[Route('/update/{id}', name: 'app_tag_update', methods: ["GET", "POST"])]
    #[IsGranted('ROLE_ADMIN')]
    public function update(#[\SensitiveParameter] Tag $tag, EntityManagerInterface $entityManager, Request $request): Response
    {
        $form = $this->createForm(type: TagItemType::class, data: $tag);
        $form->handleRequest(request: $request);

        if($form->isSubmitted() && $form->isValid()){
            $entityManager->persist($form->getData());
            $entityManager->flush();

            return $this->redirectToRoute(
                route: 'app_admin_index'
            );
        }

        return $this->render(
            view: 'tag/update.html.twig',
            parameters: [
                'form' => $form
            ]);
    }

}
