<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Repository\TicketRepository;
use App\Service\Search\Search;
use Doctrine\ORM\EntityManagerInterface;
use SensitiveParameter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/ticket')]
class TicketController extends AbstractController {

    #[Route('/', name: 'ticket_index', methods: ["GET"])]
    public function index(TicketRepository $ticketRepository): Response
    {
        $tickets = $ticketRepository->findBy([], ["created_at" => "DESC"]);

        return $this->render(
            view: 'ticket/index.html.twig',
            parameters: [
                'tickets' => $tickets
            ]
        );
    }

    #[Route('/show/{id}', name: 'ticket_read', methods: ["GET", "POST"])]
    public function read(#[SensitiveParameter] Ticket $ticket, EntityManagerInterface $em, Request $request): Response
    {
        return $this->render('ticket/read.html.twig', [
            'ticket' => $ticket,
        ]);
    }

    #[Route('/create', name: 'ticket_create', methods: ["GET", "POST"])]
    #[IsGranted('ROLE_USER')]
    public function create(): Response
    {
        $ticket = new Ticket();
        return $this->render(
            view: 'ticket/create.html.twig',
            parameters: [
                'ticket' => $ticket,
            ]
        );
    }

    #[Route('/update/{id}', name: 'ticket_update', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    #[IsGranted('edit', 'ticket')]
    public function update(#[SensitiveParameter] Ticket $ticket): Response
    {
        return $this->render(
            view: 'ticket/update.html.twig',
            parameters: [
                'ticket' => $ticket
        ]);
    }

    #[Route('/delete/{id}', name: 'app_ticket_delete')]
    #[IsGranted('ROLE_USER')]
    #[IsGranted('delete', 'ticket')]
    public function delete(#[SensitiveParameter] Ticket $ticket, EntityManagerInterface $entityManager): Response
    {
        try {
            $entityManager->remove($ticket);
            $entityManager->flush();

            $this->addFlash(
                type: 'success',
                message: 'your ticket has been successfully deleted'
            );
        } catch (\Exception|\Throwable $exception) {
            $this->addFlash(
                type: 'error',
                message: 'Something went wrong, we could not delete your ticket.'
            );
        } finally {
            return $this->redirectToRoute(
                route: 'ticket_index'
            );
        }
    }

    #[Route('/resolve/{id}', name: 'ticket_resolve')]
    #[IsGranted('ROLE_USER')]
    #[IsGranted('resolve', 'ticket')]
    public function resolve(#[SensitiveParameter] Ticket $ticket, EntityManagerInterface $entityManager): Response
    {
        try {
            $ticket->setIsDone(true);
            $entityManager->persist($ticket);
            $entityManager->flush();

            $this->addFlash(
                type: 'success',
                message: 'This ticket has been marked as resolved'
            );
        } catch (\Exception|\Throwable $exception) {
            $this->addFlash(
                type:'error',
                message: "You can't mark this ticket as resolved"
            );
        } finally {
            return $this->redirectToRoute(
                route: 'ticket_read',
                parameters: [
                    'id' => $ticket->getId()
            ]);
        }
    }

    #[Route('/search', name: 'app_ticket_search', methods: ["POST"])]
    public function search(Request $request, TicketRepository $ticketRepository, Search $search): Response
    {

        $tickets = $ticketRepository->findBy([], ["created_at" => "DESC"]);

        $results = $search->search(
            query: $request->request->all()["search"]["search"],
            tickets: $tickets
        );

        return $this->render('ticket/index.html.twig', [
            'tickets' => $results
        ]);
    }
}
