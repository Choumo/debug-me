<?php

namespace App\Controller;

use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('/administration')]
class AdministrationController extends AbstractController
{
    #[Route('/', name: 'app_admin_index', methods: ["GET"])]
    #[IsGranted('ROLE_ADMIN')]
    public function index(TagRepository $tagRepository): Response
    {
        $tags = $tagRepository->findAll();

        return $this->render(view: 'administration/index.html.twig', parameters: [
            'tags' => $tags
        ]);
    }
}
