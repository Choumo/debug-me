<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[Route('/user')]
class UserController extends AbstractController
{

    #[Route('/', name: 'app_user_profile', methods: ["GET"])]
    #[IsGranted('ROLE_USER')]
    public function profile(): Response
    {
        return $this->render(
            view: 'user/index.html.twig',
        );
    }

}
