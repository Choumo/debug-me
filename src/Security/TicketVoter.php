<?php

namespace App\Security;

use App\Entity\Ticket;
use App\Entity\User;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TicketVoter extends Voter
{

    const VIEW = 'view';
    const DELETE = 'delete';
    const RESOLVE = 'resolve';
    const EDIT = 'edit';
    const CREATE = 'create';

    public function __construct(private readonly Security $security)
    {

    }

    /**
     * @inheritDoc
     */
    protected function supports(string $attribute, mixed $subject): bool
    {
        if(!in_array($attribute, [self::EDIT, self::DELETE, self::RESOLVE])) {
            return false;
        }

        if(!$subject instanceof Ticket) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        //Check if user have "ROLE_ADMIN" (Admins can do anything they want)
        if($this->security->isGranted(attributes: 'ROLE_ADMIN')){
            return true;
        }

        // Check if user is logged, if not, he does not have access
        if(!$user instanceof User) {
            return false;
        }

        $ticket = $subject;

        return match ($attribute) {
            self::DELETE => $this->canDelete($ticket, $user),
            self::EDIT => $this->canEdit($ticket, $user),
            self::RESOLVE => $this->canResolve($ticket, $user),
            self::CREATE => $this->canCreate()
        };
    }

    /**
     * Check if current user is author of ticket OR if he is administrator
     * @return bool
     */
    private function canDelete(Ticket $ticket, User $user): bool
    {
        return $user === $ticket->getAuthor();
    }

    private function canEdit(Ticket $ticket, User $user): bool
    {
        return $user === $ticket->getAuthor();
    }

    /**
     * Function to check if user can mark a ticket as resolved ()
     * @param Ticket $ticket
     * @param User $user
     * @return bool
     */
    private function canResolve(Ticket $ticket, User $user): bool
    {
        return $user === $ticket->getAuthor();
    }

    /**
     * Function to check if user can access the create ticket page (they can only if they are logged in)
     * @return bool
     */
    private function canCreate(): bool
    {
        return true;
    }
}
