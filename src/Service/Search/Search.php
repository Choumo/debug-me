<?php

namespace App\Service\Search;

class Search
{
    /**
     * @param string $query
     * @param array $tickets
     * @param int $threshold
     * @return array
     */
    public function search(string $query, array $tickets, int $threshold = 3): array
    {
        $results = [];

        foreach ($tickets as $ticket) {
            $distance = levenshtein(strtolower($query), strtolower($ticket->getTitle()));

            if($distance <= $threshold){
                $results[] = $ticket;
            }
        }

        return $results;
    }
}
