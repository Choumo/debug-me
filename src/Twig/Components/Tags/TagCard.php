<?php

namespace App\Twig\Components\Tags;

use App\Entity\Tag;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
class TagCard
{
    public ?Tag $tag = null;
}
