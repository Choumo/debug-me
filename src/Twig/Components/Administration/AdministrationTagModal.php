<?php

namespace App\Twig\Components\Administration;

use App\Entity\Tag;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveArg;
use Symfony\UX\LiveComponent\Attribute\LiveListener;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
class AdministrationTagModal
{
    use DefaultActionTrait;

    public ?Tag $tag = null;

    #[LiveListener('productAdded')]
    public function incrementProductCount(#[LiveArg] Tag $product)
    {
        $this->tag = $product;
    }

}
