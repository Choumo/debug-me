<?php

namespace App\Twig\Components\Administration;

use App\Entity\Tag;
use App\Form\TagListType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\ComponentWithFormTrait;
use Symfony\UX\LiveComponent\DefaultActionTrait;
use Symfony\UX\LiveComponent\LiveCollectionTrait;

#[AsLiveComponent]
class AdministrationTagForm extends AbstractController
{

    use ComponentWithFormTrait;
    use DefaultActionTrait;
    use LiveCollectionTrait;

    #[LiveProp]
    public ?Tag $initialFormData = null;

    #[LiveProp]
    public bool $isFormValid = true;

    protected function instantiateForm(): FormInterface
    {
        return $this->createForm(TagListType::class, $this->initialFormData);
    }

    public function hasValidationErrors(): bool
    {
        $this->isFormValid = $this->getForm()->isSubmitted() && !$this->getForm()->isValid();

        return $this->isFormValid;
    }

    #[LiveAction]
    public function save(EntityManagerInterface $entityManager)
    {
        $this->submitForm();
        $data = $this->getForm()->get('tags')->getData();

        foreach($data as $tagToAdd) {
            $tag = new Tag();
            $tag->setName($tagToAdd->getName());
            $entityManager->persist($tag);
        }

        $entityManager->flush();
    }

}
