<?php

namespace App\Twig\Components\Global;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
class Modal
{
    public string $modalId;
}
