<?php

namespace App\Twig\Components\Comment;

use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;


#[AsLiveComponent]
class CommentCard
{
    use DefaultActionTrait;

    public \App\Entity\Comment $comment;

    #[LiveProp]
    public bool $isEditing = false;

    #[LiveAction]
    public function activateEditing(): void
    {
        $this->isEditing = true;
    }

}
