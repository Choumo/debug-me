<?php

namespace App\Twig\Components\Comment;

use App\Entity\Comment;
use App\Entity\Tag;
use App\Entity\Ticket;
use App\Repository\TicketRepository;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveArg;
use Symfony\UX\LiveComponent\Attribute\LiveListener;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
class CommentModal
{
    use DefaultActionTrait;

    public ?Comment $comment = null;

    public ?Ticket $ticket = null;

    #[LiveListener('updateComment')]
    public function updateComment(#[LiveArg] Comment $comment)
    {
        $this->comment = $comment;
    }
}
