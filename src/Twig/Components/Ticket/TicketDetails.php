<?php

namespace App\Twig\Components\Ticket;

use App\Entity\Ticket;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
class TicketDetails
{
    public Ticket $ticket;

}
