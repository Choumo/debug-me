<?php

namespace App\Twig\Components;

use App\Entity\Ticket;
use App\Form\TicketType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\ComponentWithFormTrait;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
class TicketForm extends AbstractController
{
    use ComponentWithFormTrait;
    use DefaultActionTrait;

    #[LiveProp]
    public bool $isFormValid = false;

    #[LiveProp]
    public ?Ticket $initialFormData = null;

    /**
     * This function is used to instanciate the form when using Symfony's Live Components
     * @return FormInterface
     */
    protected function instantiateForm(): FormInterface
    {
        return $this->createForm(TicketType::class, $this->initialFormData);
    }

    /**
     * This function is used to check if the form have any errors
     * @return bool
     */
    public function hasValidationErrors(): bool
    {
        $this->isFormValid = $this->getForm()->isSubmitted() && !$this->getForm()->isValid();

        return $this->isFormValid;
    }

    /**
     *
     * @param EntityManagerInterface $entityManager
     * @return RedirectResponse|void
     */
    #[LiveAction]
    public function create(EntityManagerInterface $entityManager)
    {
        try {
            $this->submitForm();

            $ticket = $this->getForm()->getData();

            $entityManager->persist($ticket);
            $entityManager->flush();

            $this->addFlash('success', 'Your ticket has been created !');

            return $this->redirectToRoute('ticket_index');
        }catch (\Exception|\Throwable $exception) {
            $this->addFlash('error', $exception->getMessage());
        }
    }
}
