<?php

namespace App\Twig\Components;

use App\Entity\Ticket;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
class TicketItem
{

    public Ticket $ticket;

}