<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Tag;
use App\Entity\Ticket;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;

class AppFixtures extends Fixture
{
    /**
     * This function will be used to load data inside the database
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $tagRepository = $manager->getRepository(Tag::class);

        $nbUser = rand(4, 15);              //We generate a random number between 4 and 50, that will be the number of users

        $faker = Faker\Factory::create(
            locale: 'fr_FR'
        );

        //First we create an admin account
        $user = new User();
        $user->setEmail(email: 'alexandre-admin@gmail.com');
        $user->setPassword(password: 'MyMysteri0usP4ssWord!');
        $user->setRoles(['ROLE_ADMIN']);

        $manager->persist($user);

        //We create 4 tags
        $tag = new Tag();
        $tag->setName("PHP");
        $manager->persist($tag);

        $tag = new Tag();
        $tag->setName("J*vaScript");
        $manager->persist($tag);

        $tag = new Tag();
        $tag->setName("Rust");
        $manager->persist($tag);

        $tag = new Tag();
        $tag->setName("SQL");
        $manager->persist($tag);

        //Then we create X number of users, based on the nbUser variable
        for($i = 0; $i < $nbUser; $i++){
            $user = new User();
            $user->setEmail(email: 'alexandre'.$i.'@gmail.com');
            $user->setPassword(password: 'MyMysteri0usP4ssWord!');

            $manager->persist($user);

            //Then, for this user, we create a number of tickets
            $nbUserTickets = rand(1, 20);

            for($j = 0; $j < $nbUserTickets; $j++) {
                $randomTag = rand(1, 4);

                $ticket = new Ticket();

                $ticket->setTitle(title: 'Lorem Ipsum');
                $ticket->setContent(content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse tincidunt, ante quis imperdiet convallis, nunc sem tincidunt nunc, ac elementum eros lacus eget augue. Etiam interdum iaculis urna. Donec mattis lacus a diam hendrerit, eu blandit diam malesuada. In ut nisi nulla. Vestibulum a pharetra neque, quis volutpat magna. In semper a tortor at sodales. Fusce at nisl velit. Donec venenatis vitae velit quis vehicula. Maecenas non mauris dui. Cras suscipit lectus lectus. Sed mattis est tellus, quis rutrum metus rhoncus eu.');
                $ticket->setAuthor(author: $user);
                $ticket->addTag($tagRepository->find($randomTag));

                $manager->persist($ticket);

                //Then whe create a number of comments
                $nbCommentsTicket = rand(1, 15);

                for($k = 0; $k < $nbCommentsTicket; $k++){
                    $comment = new Comment();

                    $comment->setAuthor(author: $user);
                    $comment->setTicket($ticket);
                    $comment->setContent(content: 'Lorem Impsum');

                    $manager->persist($comment);
                }
            }
        }

        $manager->flush();
    }
}
