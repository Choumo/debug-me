<?php

namespace App\Listener;

use App\Entity\Comment;
use App\Entity\Ticket;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Bundle\SecurityBundle\Security;

#[AsEntityListener(
    event: Events::prePersist,
    method: 'prePersist',
    entity: Comment::class
)]
#[AsEntityListener(
    event: Events::preUpdate,
    method: 'preUpdate',
    entity: Comment::class
)]
class CommentListener extends AbstractListener
{

    /**
     * This function is executed before a comment is persisted.
     * This function will automatically set the date and time of creation, the author of the comment
     * and add a point to this user.
     *
     * However, if we call the command "symfony console doctrine:fixtures:load", an error will be triggered because there is no logged user.
     * To prevent any error, we add a condition to check if php has been call via a CLI.
     * @param Comment $comment
     * @param PrePersistEventArgs $eventArgs
     * @return void
     */
    public function prePersist(Comment $comment, PrePersistEventArgs $eventArgs): void
    {
        $comment->setCreatedAt(
            new \DateTimeImmutable(datetime: 'now')
        );

        if('cli' != php_sapi_name()){
            $comment->setAuthor(
                author: $this->security->getUser()
            );

            $this->addPointToUser();
        }
    }

    /**
     *  This function is executed before a comment is updated.
     *  This function will add the updated date and time of the comment.
     * @param Comment $comment
     * @param PreUpdateEventArgs $eventArgs
     * @return void
     */
    public function preUpdate(Comment $comment, PreUpdateEventArgs $eventArgs): void
    {
        $comment->setUpdatedAt(
            new \DateTimeImmutable(datetime: 'now')
        );
    }
}
