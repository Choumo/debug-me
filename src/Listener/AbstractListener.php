<?php

namespace App\Listener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;

class AbstractListener {

    protected Security $security;

    protected EntityManagerInterface $entityManager;

    public function __construct(Security $security, EntityManagerInterface $entityManager) {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    /**
     * This function is used to add a point to the logged user when he create or comment a ticket.
     * @return void
     */
    public function addPointToUser()
    {
        $user = $this->security->getUser();
        $user->setActivityPoint($user->getActivityPoint() + 1);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

}
